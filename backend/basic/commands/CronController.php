<?php

namespace app\commands;

use app\models\HitBTC;
use app\models\OtskokWorker;
use app\models\RateHistory;
use app\models\TelegramUpdate;
use app\models\TelegramUser;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionIndex()
    {
        for (;;) {
            $last_seen = TelegramUpdate::getLastSeen();
            $updates = \Yii::$app->telegram->getUpdates(['offset' => $last_seen]);
            if ($updates->ok && !empty($updates->result)) {
                foreach ($updates->result as $message) {
                    $message = new TelegramUpdate(['message' => $message]);
                    $message->process();
                }
            }

            foreach (TelegramUser::find()->all() as $telegramUser) {
                $telegramUser->process();
            }
            sleep(5);
        }
    }

    public function actionGracefulPanic()
    {
        foreach (OtskokWorker::find()->all() as $worker) {
            $worker->gracefulPanic(1.001);
        }
    }

    public function actionFullPanic()
    {
        foreach (OtskokWorker::find()->all() as $worker) {
            $worker->gracefulPanic(0.95);
        }
    }

    public function actionStartOtskoker()
    {
        foreach (TelegramUser::find()->all() as $user) {
            $api = new HitBTC(['user' => $user]);
            if ($api && $api->key && $api->secret) {
                foreach (OtskokWorker::findAll(['user_id' => $user->id]) as $worker) {
                    if ($worker->state == OtskokWorker::CANCELLED) {
                        $worker->delete();
                    }
                }
                OtskokWorker::createOnce($user, $api, ['pair' => 'XRPUSDT', 'usd' => 'USD', 'cc' => 'XRP']);
            }
        }
    }

    public function actionUsers()
    {
        $total = 0;
        foreach (TelegramUser::find()->all() as $user)
        {
            if ($user->id == 2) continue;

            $key = $user->getConfig('hitbtc_key');
            $secret = $user->getConfig('hitbtc_secret');
            $maxSum = 0;

            if ($key && $secret) {
                $api = new HitBTC(['user'=>$user]);

                $cumChange = 0;
                $orders = $api->getTradeHistory();
                if (empty($orders)) continue;

                $firstOrder = strtotime($orders[0]['timestamp']);
                $maxChange = 0;

                foreach ($orders as $order) {

                    $maxSum = max($maxSum, $order['price'] * $order['quantity']);

                    if ($order['symbol'] != 'XRPUSDT') {
                        continue;
                    }
                    if ($order['clientOrderId'] == '767a227d4cfd4630a949a93767df8328') {
                        continue;
                    }
                    $change = $order['price'] * $order['quantity'] * ($order['side'] == 'buy' ? -1 : 1);
                    $cumChange += $change;
                    $maxChange = max($maxChange, $cumChange);
                }

                $totalTime = time() - $firstOrder;
                $sumMonth = ($maxChange / $totalTime) * 60 * 60 * 24 * 30;

                var_dump($user->id);
                var_dump($maxChange);
                var_dump($totalTime);
                $total += $maxChange;
                $perc = $maxChange ? $sumMonth / $maxChange * 100 : 0;
                echo "% month $perc\n";
            }
        }
        var_dump($total);
    }
}