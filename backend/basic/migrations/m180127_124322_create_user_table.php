<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180127_124322_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('telegram_user', [
            'id' => $this->primaryKey(),
            'telegram_id' => $this->integer(),
            'config' => $this->text(),
        ]);

        $this->insert('telegram_user', [
            'telegram_id' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('telegram_user');
    }
}
