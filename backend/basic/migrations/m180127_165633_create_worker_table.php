<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m180127_165633_create_worker_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'worker_class' => $this->string(200),
            'api_class' => $this->string(200),
            'data' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('worker');
    }
}
