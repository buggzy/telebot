<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rate`.
 */
class m180127_193038_create_rate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rate', [
            'id' => $this->primaryKey(),
            'timestamp' => $this->integer(),
            'rate' => $this->float(),
            'pair' => $this->string(50),
        ]);

        $this->createIndex('rate_ts', 'rate', ['timestamp']);
        $this->createIndex('rate_pair', 'rate', ['pair']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rate');
    }
}
