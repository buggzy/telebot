<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tradelog`.
 */
class m180128_073443_create_tradelog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tradelog', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'change' => $this->float(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tradelog');
    }
}
