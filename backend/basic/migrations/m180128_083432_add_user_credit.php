<?php

use yii\db\Migration;

/**
 * Class m180128_083432_add_user_credit
 */
class m180128_083432_add_user_credit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('telegram_user', 'credit', 'float');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('telegram_user', 'credit');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_083432_add_user_credit cannot be reverted.\n";

        return false;
    }
    */
}
