<?php

namespace app\models;

use yii\base\Model;

class HitBTC extends Model
{
    const CONFIG_KEY = 'hitbtc_key';
    const CONFIG_SECRET = 'hitbtc_secret';

    const ORDER_STATUS_PENDING = 0;
    const ORDER_STATUS_COMPLETED = 1;
    const ORDER_STATUS_CANCELLED = 2;
    const ORDER_STATUS_CANCELLED_PARTIAL = 3;

    public $user;

    public function apiRequest($url, $params = [], $method = "GET")
    {

        $key = $this->key;
        $secret = $this->secret;


        $ch = curl_init('https://api.hitbtc.com' . $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key . ":" . $secret);
        if ($method != "GET") {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

            $params_encoded = http_build_query($params);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params_encoded);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        curl_close($ch);
        return JSON_decode($result, true);

    }

    public function getFunds()
    {
        $result = $this->apiRequest('/api/2/trading/balance');
        if ($result['error']) {
            if ($result['error']['message'] == 'Authorization failed') {
                $this->user->reply('Invalid key or secret');
                Worker::shutdownAll($this->user->id, $this::className());
                return null;
            } elseif (isset($result['error']['message'])) {
                $this->user->reply($result['error']['message']);
                return null;
            }
        }
        $retval = [];
        if (!$result || empty($result)) {
            return null;
        }
        foreach ($result as $item) {
            $retval[$item['currency']] = $item['available'];
        }
        return $retval;
    }

    public function getConfig($key)
    {
        return $this->user->getConfig($key);
    }

    public function setConfig($key, $value)
    {
        return $this->user->setConfig($key, $value);
    }

    public function getKey()
    {
        return $this->getConfig(self::CONFIG_KEY);
    }

    public function getSecret()
    {
        return $this->getConfig(self::CONFIG_SECRET);
    }

    public function getEnabled()
    {
        return !!$this->key && !!$this->secret;
    }

    public function getBalanceText()
    {
        $funds = $this->getFunds();
        $balanceText = '';
        if ($funds) {
            foreach ($funds as $c => $fund) {
                if ($fund > 0) {
                    $balanceText .= "$c $fund\n";
                }
            }
        }
        if (empty($funds)) {
            $balanceText = 'На учетной записи нет средств (или они не переведены на trading account)';
        }
        return $balanceText;
    }

    public function getOrders()
    {
        return $this->apiRequest('/api/2/order');
    }

    public function getTicker($pairs = [])
    {
        $result = $this->apiRequest('/api/2/public/ticker');
        if (!is_array($result)) {
            return null;
        }
        $retval = [];
        foreach ($result as $item) {
            $retval[$item['symbol']] = $item;
        }
        return $retval;
    }

    public function getRate($pair)
    {
        $ticker = $this->getTicker([$pair]);
        return $ticker[$pair]['last'];
    }

    public function cancelOrder($orderId)
    {
        $result = $this->apiRequest('/api/2/order/' . $orderId, [], 'DELETE');
        var_dump($result);
        return $result;
    }

    public function orderStatus($orderId)
    {
        $result = $this->apiRequest('/api/2/order/' . $orderId);
        if (!is_array($result)) {
            return null;
        }
        if (($result['clientOrderId'] ?? 0) == $orderId) {
            return self::ORDER_STATUS_PENDING;
        }
        $result = $this->apiRequest('/api/2/history/order?clientOrderId=' . $orderId);
        if (!is_array($result)) {
            return null;
        }
        foreach ($result as $item) {
            if ($item['clientOrderId'] ?? 0 == $orderId) {
                if ($item['status'] == 'canceled') {
                    return self::ORDER_STATUS_CANCELLED;
                }
                if ($item['status'] == 'filled') {
                    return self::ORDER_STATUS_COMPLETED;
                }
            }
        }
        return null;
    }

    public function createBuyOrder($pair, $buy, $amount_buy)
    {
        $orderId = uniqid();
        $result = $this->apiRequest(
            '/api/2/order',
            [
                'clientOrderId' => $orderId,
                'symbol' => $pair,
                'quantity' => $amount_buy,
                'price' => $buy,
                'type' => 'limit',
                'side' => 'buy',
                'timeInForce' => 'GTC',
            ],
            'POST'
        );
        return $result['clientOrderId'] ?? null;
    }

    public function createSellOrder($pair, $sell, $amount_sell)
    {
        $orderId = uniqid();
        $result = $this->apiRequest(
            '/api/2/order',
            [
                'clientOrderId' => $orderId,
                'symbol' => $pair,
                'quantity' => $amount_sell,
                'price' => $sell,
                'type' => 'limit',
                'side' => 'sell',
                'timeInForce' => 'GTC',
            ],
            'POST'
        );
        return $result['clientOrderId'] ?? null;
    }

    public function getTradeHistory()
    {
        $result = $this->apiRequest('/api/2/history/trades?limit=1000&sort=ASC');
        return $result;
    }

}