<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 27.01.18
 * Time: 22:09
 */

namespace app\models;


class OtskokWorker extends Worker
{

    const STATE_BUY = 1;
    const STATE_SELL = 2;
    const CANCELLED = -1;

    public static function configAttributes()
    {
        return [
            'state',
            'peakTime',
            'lastPeak',
            'lastDno',
            'dnoLimit',
            'sellPlus',
            'pair',
            'usd',
            'cc',
            'buyOrder',
            'sellOrder',
            'boughtRate',
            'boughtAmount',
            'sellRate',
            'sellAmount',
        ];
    }

    public function showStatus()
    {
        $statusText = "Бот-отскокер\n";
        if ($this->state == self::STATE_BUY) {
            $statusText .= "Ожидаю покупки {$this->boughtAmount} по {$this->boughtRate}\n";
        }
        if ($this->state == self::STATE_SELL) {
            $statusText .= "Купили {$this->boughtAmount} по {$this->boughtRate}, продаю {$this->sellAmount} по {$this->sellRate}\n";
        }
        $this->user->reply($statusText);
    }

    public function shutdown()
    {
        $this->user->reply('Тушим бота-отскокера');
        if ($this->buyOrder) {
            $this->api->cancelOrder($this->buyOrder);
            $this->buyOrder = null;
            $this->user->reply('Отменяем ордер на покупку');
        }
        if ($this->sellOrder) {
            $this->user->reply('Ордер на продажу оставлен');
        }
        $this->state = null;
    }

    public function gracefulPanic($percent)
    {
        if ($this->buyOrder) {
            $this->api->cancelOrder($this->buyOrder);
            $this->buyOrder = null;
//            $this->user->reply('Отменяем ордер на покупку');
            echo 'Отменяем ордер на покупку для пользователя ', $this->user->id;
            $this->state = self::CANCELLED;
        }
        if ($this->sellOrder) {
            $this->api->cancelOrder($this->sellOrder);
            $funds = $this->api->getFunds();
            $cc = $funds[$this->cc];
            $amount = min($this->boughtAmount, $cc);
            $price = $this->boughtRate * $percent;
            $this->sellOrder = $this->api->createSellOrder($this->pair, $price, $amount);
            $this->sellRate = $price;
            $this->sellAmount = $amount;
//            $this->user->reply('Передвигаем ордер на продажу на минимальный профит');
            echo 'Передвигаем ордер на продажу на минимальный профит для пользователя ', $this->user->id;
            $this->state = self::CANCELLED;
        }
    }

    public function process()
    {
        if (!$this->api->enabled) {
            return false;
        }

        if ($this->state === null) {
            $this->initWorker();
        }

        $rateKey = [$this->api_class, $this->pair];

        $rate = RateHistory::getPeak($rateKey, time()-60);
        if ($rate === null) {
            $rate = $this->api->getRate($this->pair);
            if ($rate === null) {
                return null;
            }
            RateHistory::eatRate($rate, $rateKey, time());
        }

        if ($this->state == self::STATE_BUY) {
            $peak = RateHistory::getPeak($rateKey, time() - $this->peakTime);
            $dno = RateHistory::getMin($rateKey, time() - $this->peakTime * 2);
            if (!$this->buyOrder || ($this->lastPeak != $peak) || ($this->lastDno != $dno)) {
                $this->updateBuyOrder($peak, $dno);
                $this->lastPeak = $peak;
                $this->lastDno = $dno;
            }
            if ($this->buyOrder) {
                $status = $this->api->orderStatus($this->buyOrder);
                if ($status == $this->api::ORDER_STATUS_CANCELLED) {
                    $this->user->reply('Кто-то отменил заказ, прекращаем активность');
                    $this->state = self::CANCELLED;
                }

                if ($status == $this->api::ORDER_STATUS_COMPLETED) {
                    $this->user->reply('Заказ на покупку завершен, переходим к продаже');
                    TradeLog::add(['user_id' => $this->user->id, 'change' => - $this->boughtRate * $this->boughtAmount]);
                    $this->buyOrder = null;
                    $this->state = self::STATE_SELL;
                }
            }
        }

        if ($this->state == self::STATE_SELL) {
            if (!$this->sellOrder) {
                $this->updateSellOrder($this->boughtRate);
            }
            if ($this->sellOrder) {
                $status = $this->api->orderStatus($this->sellOrder);
                if ($status == $this->api::ORDER_STATUS_COMPLETED) {
                    $this->user->reply($this->api->getBalanceText());
                    $this->user->reply('Заказ на продажу завершен, переходим к покупке');
                    TradeLog::add(['user_id' => $this->user->id, 'change' => $this->sellRate * $this->sellAmount]);
                    $this->sellOrder = null;
                    $this->state = self::STATE_BUY;
                }
            }
        }

    }

    private function updateBuyOrder($peak, $dno)
    {
        if ($this->buyOrder) {
            $this->api->cancelOrder($this->buyOrder);
        }
        $priceByPeak = $peak * (1 - $this->dnoLimit);
        // защита от закупки после пампа
        $priceByDno = $dno * 1.20;

        $price = min($priceByPeak, $priceByDno);

        $funds = $this->api->getFunds();
        $usd = $funds[$this->usd];
        $amount = $usd / $price * 0.99;
        $allowed =  $this->allowedAmount() / $price;
        if ($amount < 1) {
            echo "{$this->user->id} amount = {$amount}\n";
            return null;
        }
        if ($amount > $allowed) {
            $this->user->reply("Максимальный размер торгуемой котлеты ограничен $allowed, обратитесь к @buggzy2 за подробностями");
            $amount = $allowed;
        }
        echo("{$this->user->id} Создаем заказ на покупку за $price количеством $amount\n");
        $this->buyOrder = $this->api->createBuyOrder($this->pair, $price, $amount);
        $this->boughtRate = $price;
        $this->boughtAmount = $amount;
        sleep(5); // чтобы биржа успела обработать
    }

    private function updateSellOrder($boughtRate)
    {
        $funds = $this->api->getFunds();
        $cc = $funds[$this->cc];
        $amount = min($this->boughtAmount, $cc);
        $price = $boughtRate * (1 + $this->sellPlus);
//        $this->user->reply("Создаем заказ на продажу за $price количеством $amount");
        echo("{$this->user->id} Создаем заказ на продажу за $price количеством $amount\n");
        $this->sellOrder = $this->api->createSellOrder($this->pair, $price, $amount);
        $this->sellRate = $price;
        $this->sellAmount = $amount;
    }

    private function allowedAmount()
    {
        $amount = ($this->user->credit - $this->user->tradeSum) / 0.05;
        $amount = max($amount, 100);
        return $amount;
    }


    private function initWorker()
    {
        $this->user->reply('Запускаем бота-отскокера');
        $this->state = self::STATE_BUY;
        $this->dnoLimit = 0.20 + (rand(0, 1000) / 100000);
        $this->sellPlus = 0.05;
        $this->peakTime = 60*60*12;
    }

    public function __get($name) {
        return in_array($name, self::configAttributes()) ? $this->getVal($name) : parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (in_array($name, self::configAttributes())) {
            $this->setVal($name, $value);
        } else {
            parent::__set($name, $value);
        }
    }

}