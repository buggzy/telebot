<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 28.01.18
 * Time: 0:29
 */

namespace app\models;

use yii\db\ActiveRecord;

class RateHistory extends ActiveRecord
{
    public static function tableName()
    {
        return 'rate';
    }

    private static function hashKey($key)
    {
        return md5(serialize($key));
    }

    public static function eatRate($rate, $key, $time)
    {
        $r = new self(['rate' => $rate, 'pair' => self::hashKey($key), 'timestamp' => $time]);
        $r->save();
    }

    public static function getPeak($key, $time)
    {
        return self::find()
            ->select('max(rate)')
            ->where('timestamp > ' . $time)
            ->andWhere(['pair' => self::hashKey($key)])
            ->scalar();
    }

    public static function getMin($key, $time)
    {
        return self::find()
            ->select('min(rate)')
            ->where('timestamp > ' . $time)
            ->andWhere(['pair' => self::hashKey($key)])
            ->scalar();
    }
}