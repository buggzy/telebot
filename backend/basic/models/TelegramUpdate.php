<?php

namespace app\models;

use yii\base\Model;

class TelegramUpdate extends Model
{

    const LAST_SEEN = 'last_seen';

    public $message;

    public function process()
    {
        TelegramUpdate::setLastSeen($this->message->update_id + 1);

        $user = TelegramUser::findOne(['telegram_id' => $this->message->message->from->id]) ?? new TelegramUser(['telegram_id' => $this->message->message->from->id]);

        $text = $this->message->message->text;

        if (($text == '/start') || ($text == '/help')) {
            $this->reply("Бот, зарабатывающий на торговле криптовалютой.\n\n
Для использования необходим аккаунт на одной из поддерживаемых критоповалютных бирж и API-ключ, позволяющий производить торговые операции.\n\n
В настоящий момент поддерживается HitBTC.com\n\n
Команды:\n
/hitbtc <api key> <secret key> - подключить аккаунт HitBTC\n
/hitbtc_balance - показать текущий баланс на HitBTC\n
/hitbtc_disable - отключить бота для биржи (завершает ордеры на покупку, продажу оставляет)\n
/workers - показывает статус бота (ордеры и тд)\n\n
В демо-режиме доступна торговля в пределах 100 USDT. Для начала работы на счете должны быть USDT, а не криптовалюта.");
        } elseif ($text == '/credit') {
            $userCredit = $user->credit;
            $tradeSum = $user->tradeSum;
            $this->reply("Мой код: {$user->id}:{$user->telegram_id}\nДоступный объем заработка: $userCredit\nСуммарный заработок: $tradeSum");
        } elseif ($text == '/workers') {
            $user->workers();
        } elseif (preg_match('%^/hitbtc\s+([0-9a-z]+)\s+([0-9a-z]+)%', $text, $matches)) {
            $user->setConfig(HitBTC::CONFIG_KEY, $matches[1]);
            $user->setConfig(HitBTC::CONFIG_SECRET, $matches[2]);
            $api = new HitBTC(['user' => $user]);
            $balanceText = $api->getBalanceText();
            if ($balanceText) {
                $this->reply($balanceText);
                OtskokWorker::createOnce($user, $api, ['pair' => 'XRPUSDT', 'usd' => 'USD', 'cc' => 'XRP']);
            }
        } elseif ($text == '/hitbtc_balance') {
            $balanceText = (new HitBTC(['user' => $user]))->getBalanceText();
            if ($balanceText) {
                $this->reply($balanceText);
            }
        } elseif ($text == '/hitbtc_disable') {
            Worker::shutdownAll($user, HitBTC::className());
            $user->setConfig(HitBTC::CONFIG_KEY, '');
            $user->setConfig(HitBTC::CONFIG_SECRET, '');
            $this->reply('Биржа выключена');
        } else {
            $this->reply('Команда не распознана');
        }

        $user->save(false);
    }

    public function reply($text)
    {
        \Yii::$app->telegram->sendMessage(
            [
                'chat_id' => $this->message->message->chat->id,
                'text' => $text,
            ]
        );
    }

    public static function getLastSeen()
    {
        return TelegramUser::getGlobal(self::LAST_SEEN);
    }

    public static function setLastSeen($value)
    {
        TelegramUser::setGlobal(self::LAST_SEEN, $value);
    }
}