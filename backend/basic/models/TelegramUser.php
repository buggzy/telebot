<?php

namespace app\models;

use yii\db\ActiveRecord;

class TelegramUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'telegram_user';
    }

    private static function getGlobalObj()
    {
        return self::findOne(['telegram_id' => 1]);
    }

    public function setConfig($key, $value)
    {
        $data = JSON_decode($this->config, true);
        $data[$key] = $value;
        $this->config = JSON_encode($data);
        $this->save(false, ['config']);
    }

    public function getConfig($key)
    {
        $data = JSON_decode($this->config, true);
        return $data[$key] ?? null;
    }

    public static function getGlobal($key)
    {
        return self::getGlobalObj()->getConfig($key);
    }

    public static function setGlobal($key, $value)
    {
        self::getGlobalObj()->setConfig($key, $value);
    }

    public function process()
    {
        foreach (Worker::findAll(['user_id' => $this->id]) as $worker) {
            $worker = $worker->worker_class::findOne($worker->id);
            $worker->process();
        }
    }

    public function workers()
    {
        foreach (Worker::findAll(['user_id' => $this->id]) as $worker) {
            $worker = $worker->worker_class::findOne($worker->id);
            $worker->showStatus();
        }
    }

    public function reply($text)
    {
        \Yii::$app->telegram->sendMessage(
            [
                'chat_id' => $this->telegram_id,
                'text' => $text,
            ]
        );
    }

    public function getTradeSum()
    {
        $sum = 0;
        $sumMax = 0;
        foreach (TradeLog::findAll(['user_id' => $this->id]) as $operation) {
            $sum += $operation->change;
            $sumMax = max($sum, $sumMax);
        }
        return $sumMax;
    }
}
