<?php

namespace app\models;

use yii\db\ActiveRecord;

class TradeLog extends ActiveRecord
{
    public static function tableName()
    {
        return 'tradelog';
    }

    public static function add($array)
    {
        $r = new self($array);
        $r->save();
    }
}
