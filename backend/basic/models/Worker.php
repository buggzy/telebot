<?php

namespace app\models;

use yii\db\ActiveRecord;

class Worker extends ActiveRecord {

    public static function tableName()
    {
        return 'worker';
    }

    public function getUser()
    {
        return $this->hasOne(TelegramUser::class, ['id' => 'user_id']);
    }

    public function setUser($val)
    {
        $this->user_id = $val->id;
    }

    public function getApi()
    {
        return new $this->api_class(['user' => $this->user]);
    }

    public function setApi($api)
    {
        $this->api_class = $api::className();
    }

    public static function shutdownAll($userId, $className)
    {
        foreach (self::findAll(['user_id' => $userId, 'api_class' => $className]) as $worker) {
            $worker = $worker->worker_class::findOne($worker->id);
            $worker->shutdown();
        }
    }

    public function setVal($key, $val)
    {
        $data = JSON_decode($this->data, true) ?? [];
        $data[$key] = $val;
        $this->data = JSON_encode($data);
        $this->save(false, ['data']);
    }

    public function getVal($key)
    {
        $data = JSON_decode($this->data, true) ?? [];
        return $data[$key] ?? null;
    }

    // дефолтовые функции-загглушки, переопределяются в конкретных воркерах
    public function shutdown()
    {
    }

    public function showStatus()
    {
    }

    public function beforeSave($insert)
    {
        $this->worker_class = $this::className();
        return parent::beforeSave($insert);
    }

    public static function createOnce($user, $api, $config = [])
    {
        $instance = self::findOne(['user_id' => $user->id, 'worker_class' => static::className(), 'api_class' => $api::className()])
            ?? new static(['user' => $user, 'api' => $api]);

        foreach ($config as $key=>$val) {
            $instance->$key = $val;
        }
        $instance->save();
        return $instance;
    }

}